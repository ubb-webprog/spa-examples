import { useState } from 'react';
import { Link } from 'react-router-dom';
import { deleteQuiz } from '../api/quiz.api';
import { Quiz } from '../model/quiz.model';

type QuizListEntryProps = {
  quiz: Quiz;
};

export default function QuizListEntry({ quiz }: QuizListEntryProps) {
  const [deleted, setDeleted] = useState(false);

  const onDeleteQuiz = () => {
    // eslint-disable-next-line no-restricted-globals, no-alert
    if (confirm(`Are you sure you want to delete quiz ${quiz.name}?`)) {
      deleteQuiz(quiz.id).then(() => setDeleted(true));
    }
  };

  if (deleted) {
    return <></>;
  }

  return (
    <div className="quiz">
      <h2>
        <Link to={`/quizzes/${quiz.id}`}>{quiz.name}</Link>
        <input className="menu-button" type="button" value="Delete" onClick={onDeleteQuiz} />
      </h2>
      <div className="date">{quiz.date}</div>
      <div>{quiz.points} pts</div>
    </div>
  );
}
