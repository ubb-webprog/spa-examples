import { CreateQuiz, Quiz } from '../model/quiz.model';

export const apiBase = `http://${window.location.hostname}:8080/api`;

export async function findAllQuizes(): Promise<Quiz[]> {
  const response = await fetch(`${apiBase}/quizes`);
  const quizzes = await response.json();
  return quizzes;
}

export async function findQuizById(id: string): Promise<Quiz> {
  const response = await fetch(`${apiBase}/quizes/${id}`);
  const quiz = await response.json();
  return quiz;
}

export async function insertQuiz(createQuiz: CreateQuiz): Promise<Quiz> {
  const response = await fetch(`${apiBase}/quizes`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(createQuiz),
  });
  const quiz = await response.json();
  return quiz;
}

export async function deleteQuiz(id: string): Promise<void> {
  await fetch(`${apiBase}/quizes/${id}`, {
    method: 'DELETE',
  });
}
