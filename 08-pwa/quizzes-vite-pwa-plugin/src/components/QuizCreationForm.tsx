import { useCallback, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { insertQuiz } from '../api/quiz.api';
import { CreateQuiz } from '../model/quiz.model';

export default function QuizCreationForm() {
  const [name, setName] = useState<string>('');
  const [date, setDate] = useState<string>('');
  const [points, setPoints] = useState<number>(42);
  const [description, setDescription] = useState<string>('');

  const navigate = useNavigate();

  const submitForm = useCallback(async () => {
    const createQuiz: CreateQuiz = { name, date, points, description };
    // létrehozzuk a kvízt, siker esetén átirányítunk a részleteire
    const quiz = await insertQuiz(createQuiz);
    navigate(`/quizzes/${quiz.id}`);
  }, [name, date, points, description, navigate]);

  return (
    <>
      <h1>Create Quiz</h1>

      <label htmlFor="name">
        Quiz Name:&nbsp;
        <input
          id="name"
          type="text"
          name="name"
          placeholder="Quiz Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
      </label>

      <br />

      <label htmlFor="date">
        Date:&nbsp;
        <input
          id="date"
          type="datetime-local"
          name="date"
          value={date.toLocaleString()}
          onChange={(e) => setDate(e.target.value)}
        />
      </label>

      <br />

      <label htmlFor="point">
        Points:&nbsp;
        <input
          id="point"
          type="number"
          name="point"
          value={points}
          onChange={(e) => setPoints(Number(e.target.value))}
        />
      </label>

      <br />

      <label htmlFor="description">
        Description:&nbsp;
        <textarea
          name="description"
          id="description"
          cols={30}
          rows={10}
          placeholder="Description..."
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />
      </label>

      <br />

      <input className="menu-button" type="button" value="Submit" onClick={submitForm} />
    </>
  );
}
