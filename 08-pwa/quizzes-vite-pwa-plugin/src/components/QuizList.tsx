import { useEffect, useState } from 'react';
import { findAllQuizes } from '../api/quiz.api';
import { Quiz } from '../model/quiz.model';
import QuizListEntry from './QuizListEntry';

export default function QuizList() {
  const [quizzes, setQuizzes] = useState<Quiz[]>([]);

  // quizek betöltése első mountkor
  useEffect(() => {
    (async () => {
      const newQuizzes = await findAllQuizes();
      setQuizzes(newQuizzes);
    })();
  }, []);

  return (
    <>
      <h1>Quizes</h1>
      <div id="container">
        {quizzes.map((quiz) => (
          <QuizListEntry key={quiz.id} quiz={quiz} />
        ))}
      </div>
    </>
  );
}
