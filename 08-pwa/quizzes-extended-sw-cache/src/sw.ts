/* eslint-disable no-underscore-dangle */
// TS-nek való jelzés, hogy ez a file workerként értelmezendő
/// <reference lib="WebWorker" />

import { clientsClaim } from 'workbox-core';
import { cleanupOutdatedCaches, createHandlerBoundToURL, precacheAndRoute } from 'workbox-precaching';
import { NavigationRoute, registerRoute } from 'workbox-routing';
import { apiBase } from './api/quiz.api';

// a self (this) változó itt más típusú mint a nem-SW JS-ekben
declare const self: ServiceWorkerGlobalScope & typeof globalThis;

//
// saját service worker logika (nem workbox)
//

async function cacheAllQuizzes() {
  // előre cache-eljük az API hívást
  const cache = await caches.open('quizzes');
  await cache.add(`${apiBase}/quizes`);
  console.log('[Service Worker] Cached quizzes API response eagerly');
}

// SW telepítéskor hívódik meg
self.addEventListener('install', (event) => {
  console.log('[Service Worker] Installing SW');
  event.waitUntil(cacheAllQuizzes());
});

async function tryQuizzesCache(request: Request) {
  // megnézzük, hogy a cache-ben van-e a request
  const cache = await caches.open('quizzes');
  let response = await cache.match(request);
  if (response) {
    console.log('[Service Worker] Returning from cache: ', request.url);
    return response;
  }

  // ha nem, "next"
  response = await fetch(request);
  // cache-eljük ezt az információt későbbre (a res egyszer olvasható, ezért klónozzuk)
  await cache.put(request, response.clone());
  return response;
}

// fetch middleware
self.addEventListener('fetch', (event) => {
  // csak az API-ra küldött kérésekre van middleware
  if (event.request.url.startsWith(apiBase)) {
    event.respondWith(tryQuizzesCache(event.request));
  }
});

//
// Workbox metódusok
//

// forszírozva állítsuk a jelenlegi SW-t mint "aktív"
self.skipWaiting();
clientsClaim();

// "asset"-ek cache-elése
precacheAndRoute(self.__WB_MANIFEST);

// régi (workbox) cache-ek törlése
cleanupOutdatedCaches();

// ?
registerRoute(new NavigationRoute(createHandlerBoundToURL('index.html')));
