export type Quiz = {
  id: string;
  name: string;
  date: string;
  points: number;
  description: string;
};

export type CreateQuiz = Omit<Quiz, 'id'>;
