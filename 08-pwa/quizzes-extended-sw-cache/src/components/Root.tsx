import { BrowserRouter, Route, Routes } from 'react-router-dom';

import About from './About';
import Footer from './Footer';
import Home from './Home';
import NavBar from './NavBar';
import QuizCreationForm from './QuizCreationForm';
import QuizDetails from './QuizDetails';
import QuizList from './QuizList';

export default function Root() {
  return (
    <BrowserRouter>
      {/* navbar minden oldal tetején */}
      <NavBar />

      {/* főoldal route-ok szerint */}
      <main>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/quizzes" element={<QuizList />} />
          <Route path="/create_quiz" element={<QuizCreationForm />} />
          <Route path="/quizzes/:quizId" element={<QuizDetails />} />
          <Route path="*" element={<i>Cannot find page</i>} />
        </Routes>
      </main>

      {/* lábléc minden oldal alján */}
      <Footer />
    </BrowserRouter>
  );
}
