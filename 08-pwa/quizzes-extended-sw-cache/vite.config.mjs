import react from '@vitejs/plugin-react';
import { defineConfig } from 'vite';
import { VitePWA } from 'vite-plugin-pwa';

const reactPlugin = react();

const pwaPlugin = VitePWA({
  registerType: 'autoUpdate',
  name: 'Quizzes',
  strategies: 'injectManifest',
  srcDir: 'src',
  filename: 'sw.ts',
  devOptions: {
    enabled: process.env.DEV === 'true',
  },
  manifest: {
    icons: [
      {
        src: '/icons/icon-512.png',
        type: 'image/png',
        sizes: '512x512',
      },
      {
        src: '/icons/icon-128.png',
        type: 'image/png',
        sizes: '128x128',
      },
      {
        src: '/icons/icon-32.png',
        type: 'image/png',
        sizes: '32x32',
      },
    ],
  },
});

const config = defineConfig({
  plugins: [reactPlugin, pwaPlugin],
});

export default config;
