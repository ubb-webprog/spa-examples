import { useCallback, useEffect } from 'react';

export default function About() {
  useEffect(() => {
    // kérünk engedélyt értesítésekre
    Notification.requestPermission().then((result) => {
      console.log('Notification permission', result);
    });
  }, []);

  // gombnyomásra küldünk egy értesítést
  const showLocalNotification = useCallback(async () => {
    const registration = await navigator.serviceWorker.ready;
    registration.showNotification('Welcome!', {
      body: 'Hello, world!',
    });
  }, []);

  return (
    <>
      <h1>About</h1>
      <p>This is the project</p>
      <input type="button" onClick={showLocalNotification} value="Show notification" />
    </>
  );
}
