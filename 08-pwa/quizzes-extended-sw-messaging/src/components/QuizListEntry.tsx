import { useCallback, useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { deleteQuiz } from '../api/quiz.api';
import { Quiz } from '../model/quiz.model';

type QuizListEntryProps = {
  quiz: Quiz;
};

export default function QuizListEntry({ quiz }: QuizListEntryProps) {
  const [deleted, setDeleted] = useState(false);

  useEffect(() => {
    // kérünk engedélyt értesítésekre
    Notification.requestPermission().then((result) => {
      console.log('Notification permission', result);
    });
  }, []);

  useEffect(() => {
    // ha a SW jelez, hogy törlődött a jelenlegi entitás,
    // töröljük a UI-ból is
    const onSWDeleteEvent = (event: MessageEvent) => {
      if (event.data.type === 'delete-quiz' && event.data.quizId === quiz.id) {
        console.log(`Quiz ${quiz.name} deleted through service worker`);
        setDeleted(true);
      }
    };

    navigator.serviceWorker.addEventListener('message', onSWDeleteEvent);
    return () => {
      navigator.serviceWorker.removeEventListener('message', onSWDeleteEvent);
    };
  }, []);

  const onDeleteQuiz = useCallback(async () => {
    // eslint-disable-next-line no-restricted-globals, no-alert
    if (confirm(`Are you sure you want to delete quiz ${quiz.name}?`)) {
      // szerveren való törlés
      await deleteQuiz(quiz.id);
      // kiküljük a SW-nek, hogy sikeresen töröltünk
      navigator.serviceWorker.controller?.postMessage({
        type: 'delete-quiz',
        quizId: quiz.id,
      });
      setDeleted(true);
    }
  }, [quiz.name, quiz.id]);

  if (deleted) {
    return <></>;
  }

  return (
    <div className="quiz">
      <h2>
        <Link to={`/quizzes/${quiz.id}`}>{quiz.name}</Link>
        <input className="menu-button" type="button" value="Delete" onClick={onDeleteQuiz} />
      </h2>
      <div className="date">{quiz.date}</div>
      <div>{quiz.points} pts</div>
    </div>
  );
}
