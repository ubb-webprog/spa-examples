/* eslint-disable no-underscore-dangle */
// TS-nek való jelzés, hogy ez a file workerként értelmezendő
/// <reference lib="WebWorker" />

import { clientsClaim } from 'workbox-core';
import { cleanupOutdatedCaches, createHandlerBoundToURL, precacheAndRoute } from 'workbox-precaching';
import { NavigationRoute, registerRoute } from 'workbox-routing';

// a self (this) változó itt más típusú mint a nem-SW JS-ekben
declare const self: ServiceWorkerGlobalScope & typeof globalThis;

//
// saját service worker logika (nem workbox)
//

self.addEventListener('message', async (event) => {
  const source = event.source as Client;
  console.log('[Service Worker] broadcasting message', source.id, event.data);
  if (source && source.id) {
    if (event.data.type === 'delete-quiz') {
      const activeClients = await self.clients.matchAll();
      activeClients.forEach((client) => {
        if (client.id !== (event.source as Client)?.id) {
          client.postMessage(event.data);
        }
      });

      self.registration.showNotification('Deletion', {
        body: `Quiz ${event.data.quizId} has been deleted`,
      });
    }
  }
});

//
// Workbox metódusok
//

// forszírozva állítsuk a jelenlegi SW-t mint "aktív"
self.skipWaiting();
clientsClaim();

// "asset"-ek cache-elése
precacheAndRoute(self.__WB_MANIFEST);

// régi (workbox) cache-ek törlése
cleanupOutdatedCaches();

// ?
registerRoute(new NavigationRoute(createHandlerBoundToURL('index.html')));
