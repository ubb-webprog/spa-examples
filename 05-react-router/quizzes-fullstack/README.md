# MERN full-stack példa

## 0. MongoDB

- MongoDB adatbázis felhasználónév és jelszó nélkül, `localhost:27017`-en elérhető
- a repó `tools` gyökerében található `docker-compose` segédállomány, és `Make` parancsok, hogy gyorsan elindítsuk
```bash
    cd ../../tools
    make start-mongo
```

## 1. API szerver

- express + MongoDB API szerver, quizeket tart karban
- nem használ sablonmotrot, mindig JSON-t térít vissza
- aszinkron hívásokkal kell elérni
- a `cors` modul segítségével elérhető aszinkron hívásokkal más útvonalról is, mint ahova a szerver ki van telepítve (ebben az esetben a React kliensről)
- használat:
```bash
    cd api
    npm install
    npm start
```
- endpoints:
    - `GET http://localhost:8080/api/quizes` - list of quizes
    - `GET http://localhost:8080/api/quizes/1` - quiz with given ID with description, 404 if missing
    - `POST http://localhost:8080/api/quizes` - create quiz based on JSON body
    - `DELETE http://localhost:8080/api/quizes/1` - delete quiz with given ID

## 2. React kliensoldal

- React komponensek -- JSX kód, amely aszinkron hívásokkal kommunikál az API szerverrel
- `react-query` elősegítette SPA
- használat:
```bash
    cd web
    npm install
    npm start
```
- elérés: `http://localhost:5173`
