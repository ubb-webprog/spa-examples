import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { findQuizById } from '../api/quiz.api';
import { Quiz } from '../model/quiz.model';

export default function QuizDetails() {
  const [quiz, setQuiz] = useState<Quiz>();

  // útvonal paraméter
  const { quizId } = useParams();

  // quizek betöltése első mountkor
  useEffect(() => {
    (async () => {
      if (quizId) {
        const newQuiz = await findQuizById(quizId);
        setQuiz(newQuiz);
      }
    })();
  }, [quizId]);

  // ha még nem töltődött be a quiz, loading üzenet
  if (!quiz) {
    return <i>Loading quiz...</i>;
  }

  return (
    <>
      <h1>{quiz.name}</h1>
      <div className="quiz">
        <div className="date">{quiz.date}</div>
        <div>{quiz.points} pts</div>
        <div className="description">{quiz.description}</div>
        <Link to="/">Return to main page</Link>
      </div>
    </>
  );
}
