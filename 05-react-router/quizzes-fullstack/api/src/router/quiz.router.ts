import express, { Request, Response } from 'express';
import { CreateQuiz, Quiz } from '../model/quiz.model';
import * as db from '../repo/quiz.repo';

const router = express.Router();

router.get('/', async (req, res: Response<Quiz[]>) => {
  const quizes = await db.findAllQuizes();
  res.send(quizes);
});

router.get('/:quizID', async (req, res: Response<Quiz>) => {
  const { quizID } = req.params;
  const quiz = await db.findQuizById(quizID);
  if (quiz) {
    res.send(quiz);
  } else {
    res.sendStatus(404);
  }
});

router.post('/', express.json(), async (req: Request<never, never, CreateQuiz>, res: Response<Quiz>) => {
  const quiz = await db.insertQuiz(req.body);
  res.send(quiz);
});

router.delete('/:quizID', async (req, res) => {
  const { quizID } = req.params;
  await db.deleteQuiz(quizID);
  res.sendStatus(204);
});

export default router;
