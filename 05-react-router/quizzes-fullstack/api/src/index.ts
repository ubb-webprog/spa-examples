import cors from 'cors';
import express from 'express';
import morgan from 'morgan';
import quizRouter from './router/quiz.router';

const app = express();

app.use(cors());
app.use(morgan('tiny'));
app.use('/api/quizes', quizRouter);

app.listen(8080, () => {
  console.log('Server listening on http://localhost:8080/');
});
