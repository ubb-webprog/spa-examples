// példaentitás - 10 inkrementális objektum
export const entities = [...Array(10)].map((_elem, index) => ({
  id: index,
  name: `Entity ${index}`,
}));
