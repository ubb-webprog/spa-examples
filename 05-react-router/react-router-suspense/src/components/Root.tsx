import { Suspense, lazy } from 'react';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import './style.css';

const About = lazy(() => import('./About'));
const Dashboard = lazy(() => import('./Dashboard'));
const Details = lazy(() => import('./Details'));
const Home = lazy(() => import('./Home'));
const NoMatch = lazy(() => import('./NoMatch'));

export default function Root() {
  return (
    <BrowserRouter>
      {/* navbar */}
      <nav>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/dashboard">Dashboard</Link>
        <Link to="/missing">Missing path</Link>
      </nav>
      <hr />
      <main>
        {/* URL szerint egy Route-ot rajzol ki a törzsbe */}
        <Routes>
          <Route
            path="/"
            element={
              <Suspense fallback={<p>Loading page</p>}>
                <Home />
              </Suspense>
            }
          />
          <Route
            path="/about"
            element={
              <Suspense fallback={<p>Loading page</p>}>
                <About />
              </Suspense>
            }
          />
          <Route
            path="/dashboard"
            element={
              <Suspense fallback={<p>Loading page</p>}>
                <Dashboard />
              </Suspense>
            }
          />
          <Route
            path="/details/:entityId"
            element={
              <Suspense fallback={<p>Loading page</p>}>
                <Details />
              </Suspense>
            }
          />
          <Route
            path="*"
            element={
              <Suspense fallback={<p>Loading page</p>}>
                <NoMatch />
              </Suspense>
            }
          />
        </Routes>
      </main>
    </BrowserRouter>
  );
}
