import { Link } from 'react-router-dom';
import { entities } from '../repo/entities';

export default function Dashboard() {
  return (
    <>
      <h1>Dashboard</h1>
      <p>This is the dashboard</p>
      <ul>
        {entities.map((entity) => (
          <li key={`${entity.id}`}>
            <Link to={`/details/${entity.id}`}>Details for entity {entity.name}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}
