import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

/**
 * Komponens amit kirajzolunk, ha más útvonal
 * nem található a kérésünkre
 */
export default function NoMatch() {
  const location = useLocation();
  const navigate = useNavigate();

  // átirányítás a főoldalra 5 másodperc után
  useEffect(() => {
    // időzítő beállítása 5 másodpercre
    const timeout = setTimeout(() => {
      navigate('/');
    }, 5000);

    // ha a komponens törlődik mielőtt az átirányítás
    // megtörént, lezárjuk az időzítőt
    return () => clearTimeout(timeout);
  }, [navigate]);

  return (
    <h3 className="error">
      Could not find page <i>{location.pathname}</i>, redirecting to home page...
    </h3>
  );
}
