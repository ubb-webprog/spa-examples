import { Link, useParams } from 'react-router-dom';
import { entities } from '../repo/entities';

export default function Details() {
  // path parameter kiolvasása
  const { entityId } = useParams();

  // entitás kinyerése
  const entity = entities[Number(entityId)];

  if (!entity) {
    return <p className="error">Could not find entity with ID {entityId}</p>;
  }

  return (
    <>
      <h1>Details of entity {entityId}</h1>
      <p>Name: {entity.name}</p>
      <p>
        <Link to="/dashboard">&lt; Back</Link>
      </p>
    </>
  );
}
