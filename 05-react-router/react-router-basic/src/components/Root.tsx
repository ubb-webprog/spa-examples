import { useEffect } from 'react';
import { BrowserRouter, Link, Route, Routes, useLocation, useNavigate, useParams } from 'react-router-dom';
import './style.css';

export function Home() {
  return (
    <>
      <h1>Home</h1>
      <p>Home page</p>
    </>
  );
}

export function About() {
  return (
    <>
      <h1>About</h1>
      <p>This is the project</p>
    </>
  );
}

// példaentitás - 10 inkrementális objektum
const entities = [...Array(10)].map((_elem, index) => ({
  id: index,
  name: `Entity ${index}`,
}));

export function Dashboard() {
  return (
    <>
      <h1>Dashboard</h1>
      <p>This is the dashboard</p>
      <ul>
        {entities.map((entity) => (
          <li key={`${entity.id}`}>
            <Link to={`/details/${entity.id}`}>Details for entity {entity.name}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}

export function Details() {
  // path parameter kiolvasása
  const { entityId } = useParams();

  // entitás kinyerése
  const entity = entities[Number(entityId)];

  if (!entity) {
    return <p className="error">Could not find entity with ID {entityId}</p>;
  }

  return (
    <>
      <h1>Details of entity {entityId}</h1>
      <p>Name: {entity.name}</p>
      <p>
        <Link to="/dashboard">&lt; Back</Link>
      </p>
    </>
  );
}

/**
 * Komponens amit kirajzolunk, ha más útvonal
 * nem található a kérésünkre
 */
export function NoMatch() {
  const location = useLocation();
  const navigate = useNavigate();

  // átirányítás a főoldalra 5 másodperc után
  useEffect(() => {
    // időzítő beállítása 5 másodpercre
    const timeout = setTimeout(() => {
      navigate('/');
    }, 5000);

    // ha a komponens törlődik mielőtt az átirányítás
    // megtörént, lezárjuk az időzítőt
    return () => clearTimeout(timeout);
  }, [navigate]);

  return (
    <h3 className="error">
      Could not find page <i>{location.pathname}</i>, redirecting to home page...
    </h3>
  );
}

export default function Root() {
  return (
    <BrowserRouter>
      {/* navbar */}
      <nav>
        <Link to="/">Home</Link>
        <Link to="/about">About</Link>
        <Link to="/dashboard">Dashboard</Link>
        <Link to="/missing">Missing path</Link>
      </nav>
      <hr />
      <main>
        {/* URL szerint egy Route-ot rajzol ki a törzsbe */}
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/details/:entityId" element={<Details />} />
          <Route path="*" element={<NoMatch />} />
        </Routes>
      </main>
    </BrowserRouter>
  );
}
