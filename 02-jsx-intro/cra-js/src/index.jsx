// React import szükséges ha 18-asnál régebbi a verzió
import { createRoot } from 'react-dom/client';

const element = <h1>Hello from React</h1>;
const root = document.getElementById('root');

createRoot(root).render(element);
