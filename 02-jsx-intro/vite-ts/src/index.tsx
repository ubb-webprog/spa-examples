// React import szükséges ha 18-asnál régebbi a verzió
import { createRoot } from 'react-dom/client';

const msg = 'Hello from React+TS+Vite';
const element: JSX.Element = <h1>{msg}</h1>;
const root = document.getElementById('root');

if (!root) {
  throw new Error('Could not find element to render');
}

createRoot(root).render(element);
