import { createContext } from 'react';

/**
 * A TS típus amit a context tárol
 */
export type SearchTermContextType = {
  query: string;
};

/**
 * A context amit használhatunk provider-consumer komponenseknek
 * vagy a useContext hookban
 */
export const SearchTermContext = createContext<SearchTermContextType>({
  query: 'TypeScript',
});
