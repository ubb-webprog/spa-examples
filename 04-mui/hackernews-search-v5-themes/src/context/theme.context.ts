import { createContext, CSSProperties, useContext } from 'react';

// 2 témát definiálunk
export type Theme = 'dark' | 'light';

// context definiálása
export const ThemeContext = createContext<Theme>('dark');

// színek definiálása témánként
export type ColorType = 'textColor' | 'bgColor' | 'headerTextColor' | 'headerBgColor' | 'linkColor';

export const themeColors: Record<Theme, Record<ColorType, CSSProperties['color']>> = {
  dark: {
    textColor: 'snow',
    bgColor: '#242424',
    headerTextColor: 'black',
    headerBgColor: 'lightcyan',
    linkColor: 'lightblue',
  },
  light: {
    textColor: 'black',
    bgColor: 'snow',
    headerTextColor: '#fff0f0',
    headerBgColor: 'navy',
    linkColor: 'teal',
  },
};

// hookok amivel bekérhetünk  színeket a jelenlegi témából

export function useColors() {
  const theme = useContext(ThemeContext);
  return themeColors[theme];
}

export function useColor(key: ColorType) {
  const theme = useContext(ThemeContext);
  return themeColors[theme][key];
}
