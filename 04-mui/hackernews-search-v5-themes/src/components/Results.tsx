import useHackerNewsHits from '../hooks/useHackerNewsHits';
import { Link } from './Link';

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function Results() {
  // saját hook használata
  const { hits, loading } = useHackerNewsHits();

  return (
    <>
      {loading && <i>Loading...</i>}
      <ul>
        {hits.map((item) => (
          <li key={item.objectID}>
            <Link href={item.url}>{item.title}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}
