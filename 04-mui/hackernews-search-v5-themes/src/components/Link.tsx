import { ReactNode } from 'react';
import { useColor } from '../context/theme.context';

type Props = {
  href: string;
  children: ReactNode;
};

export function Link({ href, children }: Props) {
  const linkColor = useColor('linkColor');

  return (
    <a href={href} style={{ color: linkColor }}>
      {children}
    </a>
  );
}
