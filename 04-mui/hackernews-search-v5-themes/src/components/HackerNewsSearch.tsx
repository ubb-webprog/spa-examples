import { useMemo, useState } from 'react';
import SearchField from './SearchField';
import { SearchTermContext } from '../context/searchterm.context';
import Results from './Results';
import Header from './Header';
import { useColors } from '../context/theme.context';

import './HackerNewsSearch.css';

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function HackerNewsSearch() {
  // jelenlegi keresési fogalom, állapotban tartjuk
  const [query, setQuery] = useState('TypeScript');

  // a context jelenlegi értékét memoizáljuk
  const context = useMemo(() => ({ query }), [query]);

  // témából való színlekérés
  const { textColor, bgColor } = useColors();

  return (
    <div
      style={{
        minHeight: '100vh',
        color: textColor,
        backgroundColor: bgColor,
        padding: 0,
        fontSize: 16,
        fontFamily: 'Arial',
      }}
    >
      <Header text="Hacker News Search V5" />

      <div style={{ padding: 20 }}>
        <SearchField query={query} setQuery={setQuery} />

        <SearchTermContext.Provider value={context}>
          <Results />
        </SearchTermContext.Provider>
      </div>
    </div>
  );
}
