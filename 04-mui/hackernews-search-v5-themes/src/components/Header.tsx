import { CSSProperties } from 'react';
import { useColors } from '../context/theme.context';

type Props = {
  text: string;
};

export default function Header({ text }: Props) {
  const { headerBgColor, headerTextColor } = useColors();

  const headerStyle: CSSProperties = {
    color: headerTextColor,
    backgroundColor: headerBgColor,
    marginTop: 0,
    padding: 18,
    fontFamily: 'Arial',
  };

  return <h1 style={headerStyle}>{text}</h1>;
}
