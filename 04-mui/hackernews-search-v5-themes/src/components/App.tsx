import { useState } from 'react';
import { Theme, ThemeContext } from '../context/theme.context';
import HackerNewsSearch from './HackerNewsSearch';
import { SwitchThemeIcon } from './SwitchThemeIcon';

export default function App() {
  const [theme, setTheme] = useState<Theme>('light');

  return (
    <ThemeContext.Provider value={theme}>
      <div
        aria-label="Switch theme"
        style={{ position: 'fixed', cursor: 'pointer', bottom: 10, right: 10 }}
        onClick={() => setTheme(theme === 'dark' ? 'light' : 'dark')}
        role="button"
        tabIndex={0}
      >
        <SwitchThemeIcon />
      </div>
      <HackerNewsSearch />
    </ThemeContext.Provider>
  );
}
