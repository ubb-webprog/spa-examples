import { ChangeEvent, useCallback } from 'react';

type Props = {
  query: string;
  setQuery: (newValue: string) => unknown;
};

export default function SearchField({ query, setQuery }: Props) {
  // cache-elt callback függvény
  const onQueryChange = useCallback((event: ChangeEvent<HTMLInputElement>) => setQuery(event.target.value), [setQuery]);

  return (
    <label htmlFor="searchHN">
      Search HackerNews articles:&nbsp;
      <input id="searchHN" type="text" value={query} onChange={onQueryChange} />
    </label>
  );
}
