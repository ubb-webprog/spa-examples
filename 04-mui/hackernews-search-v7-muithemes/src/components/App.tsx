import Brightness6Icon from '@mui/icons-material/Brightness6';
import { CssBaseline, IconButton, ThemeOptions, ThemeProvider, createTheme } from '@mui/material';
import { blue } from '@mui/material/colors';
import { useMemo, useState } from 'react';
import HackerNewsSearch from './HackerNewsSearch';

export type ThemeType = 'dark' | 'light';

export const themeOptionsByType: Record<ThemeType, ThemeOptions> = {
  dark: {
    palette: {
      mode: 'dark',
      secondary: {
        main: blue[200],
      },
    },
  },
  light: {
    palette: {
      mode: 'light',
      secondary: {
        main: blue[900],
      },
    },
  },
};

export default function App() {
  // 2 tématípus ami között választhatunk
  const [themeType, setThemeType] = useState<ThemeType>('light');

  // a témák felépítése ezalapján, memoizálva
  const theme = useMemo(() => {
    const themeOptions = themeOptionsByType[themeType];
    return createTheme(themeOptions);
  }, [themeType]);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <IconButton
        sx={{ position: 'fixed', cursor: 'pointer', bottom: 10, right: 10 }}
        onClick={() => setThemeType(themeType === 'dark' ? 'light' : 'dark')}
      >
        <Brightness6Icon sx={{ width: 48, height: 48 }} />
      </IconButton>
      <HackerNewsSearch />
    </ThemeProvider>
  );
}
