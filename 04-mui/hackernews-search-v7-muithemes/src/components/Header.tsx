import Typography from '@mui/material/Typography';

type Props = {
  text: string;
};

export default function Header({ text }: Props) {
  return (
    <Typography variant="h4" component="h1" align="center" sx={{ my: 0, p: 2 }}>
      {text}
    </Typography>
  );
}
