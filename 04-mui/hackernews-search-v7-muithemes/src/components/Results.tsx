import {
  Avatar,
  Box,
  CircularProgress,
  Divider,
  List,
  ListItem,
  ListItemAvatar,
  ListItemButton,
  ListItemText,
} from '@mui/material';
import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import useHackerNewsHits from '../hooks/useHackerNewsHits';

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function Results() {
  // saját hook használata
  const { hits, loading } = useHackerNewsHits();

  return (
    <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
      <Divider />
      {loading && <CircularProgress />}
      <List>
        {hits.map((item) => (
          <ListItem key={item.objectID} disablePadding>
            <ListItemButton component="a" href={item.url} target="_blank">
              <ListItemAvatar>
                <Avatar>
                  <OpenInNewIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText
                primary={item.title}
                secondary={item.author}
                secondaryTypographyProps={{ color: 'secondary' }}
              />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Divider />
    </Box>
  );
}
