import useHackerNewsHits from '../hooks/useHackerNewsHits';

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function Results() {
  // saját hook használata
  const { hits, loading } = useHackerNewsHits();

  return (
    <>
      {loading && <i>Loading...</i>}
      <ul>
        {hits.map((item) => (
          <li key={item.objectID}>
            <a href={item.url}>{item.title}</a>
          </li>
        ))}
      </ul>
    </>
  );
}
