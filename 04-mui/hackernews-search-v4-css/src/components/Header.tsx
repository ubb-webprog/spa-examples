import { CSSProperties } from 'react';

type Props = {
  text: string;
};

// a komponens stilizálása
const headerStyle: CSSProperties = {
  color: '#fff0f0',
  backgroundColor: 'navy',
  marginTop: 0,
  padding: 18,
  fontFamily: 'Arial',
};

export default function Header({ text }: Props) {
  return <h1 style={headerStyle}>{text}</h1>;
}
