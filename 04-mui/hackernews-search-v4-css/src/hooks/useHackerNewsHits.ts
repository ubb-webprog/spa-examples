import { useContext, useEffect, useState } from 'react';
import { SearchTermContext } from '../context/searchterm.context';

// https://hn.algolia.com/api
export type HackerNewsItem = {
  objectID: string;
  title: string;
  url: string;
  author: string;
  points: number;
};

/**
 * Saját hook - meglévő hookok kombinációja
 */
export default function useHackerNewsHits() {
  // context elérése hookon keresztül
  // a jelen komponens provider gyereke kell legyen
  const { query } = useContext(SearchTermContext);

  const [hits, setHits] = useState<HackerNewsItem[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true);
    fetch(`https://hn.algolia.com/api/v1/search?query=${query}`)
      .then((response) => response.json())
      .then((result) => {
        setHits(result.hits);
        setLoading(false);
      });
  }, [query]);

  return { hits, loading };
}
