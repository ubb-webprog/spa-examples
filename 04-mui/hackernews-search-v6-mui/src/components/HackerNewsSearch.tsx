import { useMemo, useState } from 'react';
import { Box, Container, CssBaseline } from '@mui/material';
import SearchField from './SearchField';
import { SearchTermContext } from '../context/searchterm.context';
import Results from './Results';
import Header from './Header';

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function HackerNewsSearch() {
  // jelenlegi keresési fogalom, állapotban tartjuk
  const [query, setQuery] = useState('TypeScript');

  // a context jelenlegi értékét memoizáljuk
  const context = useMemo(() => ({ query }), [query]);

  return (
    <>
      <CssBaseline />

      <Container maxWidth="md">
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            p: 0,
            fontSize: 16,
          }}
        >
          <Header text="Hacker News Search V6" />

          <SearchField query={query} setQuery={setQuery} />

          <SearchTermContext.Provider value={context}>
            <Results />
          </SearchTermContext.Provider>
        </Box>
      </Container>
    </>
  );
}
