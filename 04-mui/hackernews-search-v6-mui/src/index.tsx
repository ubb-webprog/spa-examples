import { createRoot } from 'react-dom/client';
import HackerNewsSearch from './components/HackerNewsSearch';

const root = document.getElementById('root');

if (!root) {
  throw new Error('Could not find root element');
}

createRoot(root).render(<HackerNewsSearch />);
