// példaentitás - 10 inkrementális objektum
const entities = [...Array(10)].map((_elem, index) => ({
  id: index,
  name: `Entity ${index}`,
}));

export default entities;
