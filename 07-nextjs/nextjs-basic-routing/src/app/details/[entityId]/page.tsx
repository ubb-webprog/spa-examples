import Link from 'next/link';
import entities from '../../../data';

type DetailsProps = {
  params: Promise<{
    entityId: string;
  }>;
};

export default async function Details({ params }: DetailsProps) {
  // path parameter kiolvasása
  // lehetne useParams hook is, de ahhoz kliensoldali komponenssé kellene avassuk ezt
  const { entityId } = await params;

  // entitás kinyerése
  const entity = entities[Number(entityId)];

  if (!entity) {
    return <p className="error">Could not find entity with ID {entityId}</p>;
  }

  return (
    <>
      <h1>Details of entity {entityId}</h1>
      <p>Name: {entity.name}</p>
      <p>
        <Link href="/dashboard">&lt; Back</Link>
      </p>
    </>
  );
}
