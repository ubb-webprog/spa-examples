import { Metadata } from 'next';
import Link from 'next/link';

import './style.css';

/**
 * metadata névvel ellátott export
 * befolyásolja a "head" tartalmát
 */
export const metadata: Metadata = {
  title: 'Next bevezető',
  description: 'Meta leírás az appról (jó SEO)',
};

/**
 * Layout komponens
 * Megadja az oldal elrendezését (minden útvonalon ugyanaz lesz)
 * Higher-order component - a jelenlegi oldalt "children" propban kapjuk meg
 */
export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body>
        <nav>
          <Link href="/">Home</Link>
          <Link href="/about">About</Link>
          <Link href="/dashboard">Dashboard</Link>
          <Link href="/missing">Missing path</Link>
        </nav>
        <hr />
        <main>{children}</main>
      </body>
    </html>
  );
}
