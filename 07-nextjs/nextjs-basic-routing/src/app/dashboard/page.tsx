import { Metadata } from 'next';
import Link from 'next/link';
import entities from '../../data';

/**
 * metadata fölülírva a layouthoz képest
 */
export const metadata: Metadata = {
  title: 'Next részletek!!',
};

export default function Dashboard() {
  return (
    <>
      <h1>Dashboard</h1>
      <p>This is the dashboard</p>
      <ul>
        {entities.map((entity) => (
          <li key={`${entity.id}`}>
            <Link href={`/details/${entity.id}`}>Details for entity {entity.name}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}
