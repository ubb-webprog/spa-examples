'use client';

import { usePathname, useRouter } from 'next/navigation';
import { useEffect } from 'react';

/**
 * Komponens amit kirajzolunk, ha más útvonal
 * nem található a kérésünkre
 */
export default function NoMatch() {
  const router = useRouter();
  const pathname = usePathname();

  // átirányítás a főoldalra 5 másodperc után
  useEffect(() => {
    // időzítő beállítása 5 másodpercre
    const timeout = setTimeout(() => {
      router.push('/');
    }, 5000);

    // ha a komponens törlődik mielőtt az átirányítás
    // megtörént, lezárjuk az időzítőt
    return () => clearTimeout(timeout);
  }, [router]);

  return (
    <h3 className="error">
      Could not find page <i>{pathname}</i>, redirecting to home page...
    </h3>
  );
}
