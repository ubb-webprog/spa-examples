import { revalidatePath } from 'next/cache';
import { redirect } from 'next/navigation';
import { CreateQuiz } from '../../model/quiz.model';
import { insertQuiz } from '../../repo/quiz.repo';

export default function QuizCreationForm() {
  /**
   * Server action példa - beágyazott aszinkron függvény,
   * ami szerveren fut s csak szerveroldali komponensben szerepelhet
   */
  async function onSubmit(formData: FormData) {
    'use server';

    const createQuiz: CreateQuiz = {
      name: String(formData.get('name')),
      date: String(formData.get('date')),
      points: Number(formData.get('points')),
      description: String(formData.get('description')),
    };

    const quiz = await insertQuiz(createQuiz);

    // töröljük a Next.js cache-t, mivel megváltozott a tartalom
    revalidatePath('/', 'layout');

    redirect(`/quizzes/${quiz.id}`);
  }

  return (
    // az "action" attribútum fogadja a formData paramú függvényt
    <form action={onSubmit}>
      <h1>Create Quiz</h1>

      <label htmlFor="name">
        Quiz Name:&nbsp;
        <input id="name" type="text" name="name" placeholder="Quiz Name" />
      </label>

      <br />

      <label htmlFor="date">
        Date:&nbsp;
        <input id="date" type="datetime-local" name="date" />
      </label>

      <br />

      <label htmlFor="points">
        Points:&nbsp;
        <input id="points" type="number" name="points" />
      </label>

      <br />

      <label htmlFor="description">
        Description:&nbsp;
        <textarea name="description" id="description" cols={30} rows={10} placeholder="Description..." />
      </label>

      <br />

      <input className="menu-button" type="submit" value="Submit" />
    </form>
  );
}
