import { Metadata } from 'next';
import Link from 'next/link';

import './style.css';

export const metadata: Metadata = {
  title: 'Next.js quizek',
};

export default function RootLayout({ children }: { children: React.ReactNode }) {
  return (
    <html lang="en">
      <body>
        <nav>
          <Link className="menu-button" href="/about">
            About
          </Link>
          <Link className="menu-button" href="/quizzes">
            Quizes
          </Link>
          <Link className="menu-button" href="/create_quiz">
            Create Quiz
          </Link>
        </nav>
        <main>{children}</main>
        <footer>
          <p>Copyright BBTE</p>
        </footer>
      </body>
    </html>
  );
}
