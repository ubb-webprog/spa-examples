import { revalidatePath } from 'next/cache';
import Link from 'next/link';
import { deleteQuiz, findAllQuizes } from '../../repo/quiz.repo';

export default async function QuizList() {
  async function onDelete(formData: FormData) {
    'use server';

    const quizId = formData.get('quizId');
    if (quizId) {
      await deleteQuiz(`${quizId}`);
      revalidatePath('/', 'layout');
    }
  }

  const quizzes = await findAllQuizes();

  return (
    <>
      <h1>Quizes</h1>
      <div id="container">
        {quizzes.map((quiz) => (
          <div className="quiz" key={quiz.id}>
            <h2>
              <Link href={`/quizzes/${quiz.id}`}>{quiz.name}</Link>
              <form action={onDelete}>
                <input type="hidden" name="quizId" value={quiz.id} />
                <input className="menu-button" type="submit" value="Delete" />
              </form>
            </h2>
            <div className="date">{quiz.date}</div>
            <div>{quiz.points} pts</div>
          </div>
        ))}
      </div>
    </>
  );
}
