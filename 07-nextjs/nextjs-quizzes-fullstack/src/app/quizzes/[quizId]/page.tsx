import Link from 'next/link';
import { notFound } from 'next/navigation';
import { findQuizById } from '../../../repo/quiz.repo';

export default async function QuizDetails({ params }: { params: Promise<{ quizId: string }> }) {
  const { quizId } = await params;
  const quiz = await findQuizById(quizId);

  if (!quiz) {
    notFound();
  }

  return (
    <>
      <h1>{quiz.name}</h1>
      <div className="quiz">
        <div className="date">{quiz.date}</div>
        <div>{quiz.points} pts</div>
        <div className="description">{quiz.description}</div>
        <Link href="/">Return to main page</Link>
      </div>
    </>
  );
}
