import { deleteQuiz, findQuizById } from '../../../../../repo/quiz.repo';

/**
 * API endpoint: GET /api/v1/quizzes/{quizId}
 * e.g.: curl -i http://localhost:3000/api/v1/quizzes/653c85b7-ebc8-4686-96e7-2a7173b56101
 */
export async function GET(request: Request, { params }: { params: Promise<{ quizId: string }> }) {
  const { quizId } = await params;
  const quiz = await findQuizById(quizId);
  if (!quiz) {
    return new Response(null, { status: 404 });
  }
  return Response.json(quiz);
}

/**
 * API endpoint: DELETE /api/v1/quizzes/{quizId}
 * e.g.: curl -X DELETE -i http://localhost:3000/api/v1/quizzes/653c85b7-ebc8-4686-96e7-2a7173b56101
 */
export async function DELETE(request: Request, { params }: { params: Promise<{ quizId: string }> }) {
  const { quizId } = await params;
  await deleteQuiz(quizId);
  return new Response(null, { status: 204 });
}
