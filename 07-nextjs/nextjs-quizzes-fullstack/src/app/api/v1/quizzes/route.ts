import { CreateQuiz } from '../../../../model/quiz.model';
import { findAllQuizes, insertQuiz } from '../../../../repo/quiz.repo';

/**
 * API endpoint: GET /api/v1/quizzes
 *
 * curl -i http://localhost:3000/api/v1/quizzes
 */
export async function GET() {
  const quizzes = await findAllQuizes();
  return Response.json(quizzes);
}

/**
 * API endpoint: POST /api/v1/quizzes
 *
 * curl -i -X POST http://localhost:3000/api/v1/quizzes --data-raw '{"name":"api","date":"2024-12-08T18:14","points":123,"description":"API description"}'
 */
export async function POST(request: Request) {
  const quizInsert: CreateQuiz = await request.json();
  const quiz = await insertQuiz(quizInsert);
  return Response.json(quiz);
}
