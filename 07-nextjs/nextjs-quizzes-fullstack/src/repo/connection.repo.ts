'use server';

import { Collection, MongoClient } from 'mongodb';
import { Quiz } from '../model/quiz.model';

// alább aszinkron módon felépített adatbázis-kapcsolat
let collection: Collection<Quiz>;

export async function getCollection(): Promise<Collection<Quiz>> {
  if (!collection) {
    const url = 'mongodb://127.0.0.1:27017';
    console.log(`Connecting to Mongo at ${url}`);
    const client = new MongoClient(url);
    await client.connect();
    console.log('Connected successfully to server');
    const db = client.db('quizzes');
    collection = db.collection('quizzes');
  }
  return collection;
}
