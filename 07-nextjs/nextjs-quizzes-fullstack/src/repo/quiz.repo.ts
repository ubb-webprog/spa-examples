'use server';

import { v4 as uuidv4 } from 'uuid';
import { CreateQuiz, Quiz } from '../model/quiz.model';
import { getCollection } from './connection.repo';

export async function findAllQuizes(): Promise<Quiz[]> {
  const collection = await getCollection();
  const result = await collection.find({}).toArray();
  return result;
}

export async function findQuizById(id: string): Promise<Quiz | null> {
  const collection = await getCollection();
  const result = await collection.findOne({ id });
  return result;
}

export async function insertQuiz(createQuiz: CreateQuiz): Promise<Quiz> {
  const collection = await getCollection();
  const quiz: Quiz = { ...createQuiz, id: uuidv4() };
  await collection.insertOne(quiz);
  return quiz;
}

export async function deleteQuiz(id: string): Promise<void> {
  const collection = await getCollection();
  await collection.deleteOne({ id });
}
