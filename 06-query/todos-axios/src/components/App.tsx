import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import CreateTodoForm from './CreateTodoForm';
import Todos from './Todos';

import './style.css';

export default function Root() {
  return (
    <BrowserRouter>
      {/* navbar */}
      <nav>
        <Link to="/todos">Todos</Link>
        <Link to="/create">Create</Link>
      </nav>
      <hr />
      <main>
        {/* URL szerint egy Route-ot rajzol ki a törzsbe */}
        <Routes>
          <Route path="/todos" element={<Todos />} />
          <Route path="/create" element={<CreateTodoForm />} />
          <Route path="*" element={<Todos />} />
        </Routes>
      </main>
    </BrowserRouter>
  );
}
