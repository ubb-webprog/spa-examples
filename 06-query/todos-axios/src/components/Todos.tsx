import { useEffect, useState } from 'react';
import { Todo, fetchAllTodos } from '../api/todos.api';

export default function Todos() {
  const [todos, setTodos] = useState<Todo[]>([]);

  useEffect(() => {
    fetchAllTodos().then(setTodos);
  }, []);

  return (
    <>
      <h1>Todos</h1>
      <ul>
        {todos.map((todo) => (
          <li key={todo.id}>
            {todo.id}.&nbsp;
            {todo.title}
            {todo.completed && <i>&nbsp;(completed)</i>}
          </li>
        ))}
      </ul>
    </>
  );
}
