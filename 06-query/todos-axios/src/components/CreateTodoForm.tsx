import { useCallback, useState } from 'react';
import { CreateTodo, Todo, createTodo } from '../api/todos.api';

export default function CreateTodoForm() {
  const [title, setTitle] = useState<string>('new todo');
  const [completed, setCompleted] = useState<boolean>(false);
  const [createdTodo, setCreatedTodo] = useState<Todo | undefined>(undefined);

  const addNewTodo = useCallback(() => {
    const todoInfo: CreateTodo = { title, completed };
    createTodo(todoInfo).then(setCreatedTodo);
  }, [title, completed]);

  return (
    <>
      <h1>Create todo</h1>
      <div>
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Title" />
      </div>
      <div>
        <input type="checkbox" checked={completed} onChange={() => setCompleted(!completed)} />
        &nbsp;Completed
      </div>
      <div>
        <input type="button" onClick={addNewTodo} value="Add new one" />
      </div>
      <div>Created: {createdTodo ? <code>{JSON.stringify(createdTodo)}</code> : <i>Nothing yet...</i>}</div>
    </>
  );
}
