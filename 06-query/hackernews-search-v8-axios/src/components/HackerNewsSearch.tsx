import { CircularProgress, Divider } from '@mui/material';
import { useState } from 'react';
import { useHackerNewsHitsAxios } from '../hooks/useHackerNewsHits';
import Results from './Results';
import SearchField from './SearchField';

export default function HackerNewsSearch() {
  const [query, setQuery] = useState('TypeScript');
  const { hits, loading } = useHackerNewsHitsAxios(query);

  return (
    <>
      <SearchField query={query} setQuery={setQuery} />
      <Divider />
      {loading ? <CircularProgress /> : <Results hits={hits} />}
    </>
  );
}
