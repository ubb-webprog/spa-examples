import SearchIcon from '@mui/icons-material/Search';
import { Box, FormControl, TextField } from '@mui/material';
import { ChangeEvent, useCallback } from 'react';

type Props = {
  query: string;
  setQuery: (newValue: string) => unknown;
};

export default function SearchField({ query, setQuery }: Props) {
  const onQueryChange = useCallback((event: ChangeEvent<HTMLInputElement>) => setQuery(event.target.value), [setQuery]);

  return (
    <FormControl sx={{ mx: 'auto', my: 2 }}>
      <Box sx={{ display: 'flex' }}>
        <SearchIcon sx={{ mr: 1, my: 1 }} />
        <TextField
          size="small"
          variant="outlined"
          label="Search HackerNews articles"
          onChange={onQueryChange}
          value={query}
        />
      </Box>
    </FormControl>
  );
}
