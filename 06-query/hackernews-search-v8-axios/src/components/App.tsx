import { Box, Container, CssBaseline } from '@mui/material';
import HackerNewsSearch from './HackerNewsSearch';
import Header from './Header';

export default function App() {
  return (
    <>
      <CssBaseline />

      <Container maxWidth="md">
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            p: 0,
            fontSize: 16,
          }}
        >
          <Header text="Hacker News Search V8" />
          <HackerNewsSearch />
        </Box>
      </Container>
    </>
  );
}
