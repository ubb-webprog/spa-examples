import axios from 'axios';
import { useEffect, useState } from 'react';

export type HackerNewsItem = {
  objectID: string;
  title: string;
  url: string;
  author: string;
  points: number;
};

export type HackerNewsPostsResponse = {
  hits: HackerNewsItem[];
};

export function useHackerNewsHitsFetch(query: string) {
  const [hits, setHits] = useState<HackerNewsItem[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const response = await fetch(`https://hn.algolia.com/api/v1/search?query=${query}`);
      const result = (await response.json()) as HackerNewsPostsResponse;
      setHits(result.hits);
      setLoading(false);
    })();
  }, [query]);

  return { hits, loading };
}

// axios példány közös beállításokkal
const hackerNewsApi = axios.create({
  // közös kiinduló URL
  baseURL: 'https://hn.algolia.com/api/v1',
  // közös headerek, pl. accept, token
  headers: {
    Accept: 'application/json',
  },
});

export function useHackerNewsHitsAxios(query: string) {
  const [hits, setHits] = useState<HackerNewsItem[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      setLoading(true);
      // axios példány használata - közös dolgokat nem kell beállítani
      // a JSON-t automatikusan deszerializálja/castolja, de NEM ellenőrzi
      // query param is fel van dolgozva megfelelően
      const response = await hackerNewsApi.get<HackerNewsPostsResponse>('/search', {
        params: { query },
      });
      setHits(response.data.hits);
      setLoading(false);
    })();
  }, [query]);

  return { hits, loading };
}
