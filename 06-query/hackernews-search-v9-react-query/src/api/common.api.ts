import axios from 'axios';

// axios példány közös beállításokkal
export const hackerNewsApi = axios.create({
  // közös kiinduló URL
  baseURL: 'https://hn.algolia.com/api/v1',
  // közös headerek, pl. accept, token
  headers: {
    Accept: 'application/json',
  },
});
