import { hackerNewsApi } from './common.api';

export type HackerNewsItem = {
  objectID: string;
  title: string;
  url: string;
  author: string;
  points: number;
};

type HackerNewsPostsResponse = {
  hits: HackerNewsItem[];
};

/**
 * API-hívás axios-szal, mint eddig
 */
export async function fetchHackerNewsHits(query: string): Promise<HackerNewsItem[]> {
  const response = await hackerNewsApi.get<HackerNewsPostsResponse>('/search', {
    params: { query },
  });
  return response.data.hits;
}
