import { useQuery } from '@tanstack/react-query';
import { fetchHackerNewsHits } from '../api/hackernews.api';

export function useHackerNewsHits(query: string) {
  // a useQuery visszaadja az aszinkron kérés állapotát
  // pl. magát az adatot, még értékelés alatt van-e, volt-e hiba
  return useQuery({
    // kulcs, amelyben az aszinkron kérés eredményét tároljuk
    queryKey: ['news', query],
    // maga az aszinkron kérés
    queryFn: () => fetchHackerNewsHits(query),
  });
}
