import { Box, Container, CssBaseline } from '@mui/material';
import { QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { queryClient } from '../query/common.query';
import HackerNewsSearch from './HackerNewsSearch';
import Header from './Header';

export default function App() {
  return (
    // react query inicializálás context API-n belül
    <QueryClientProvider client={queryClient}>
      <CssBaseline />

      <Container maxWidth="md">
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            p: 0,
            fontSize: 16,
          }}
        >
          <Header text="Hacker News Search V8" />
          <HackerNewsSearch />
        </Box>
      </Container>

      {/* segédkomponens hogy monitorizáljuk a cache tartalmát */}
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}
