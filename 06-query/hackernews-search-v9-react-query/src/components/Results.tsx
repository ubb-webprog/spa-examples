import OpenInNewIcon from '@mui/icons-material/OpenInNew';
import { Avatar, Box, Divider, List, ListItem, ListItemAvatar, ListItemButton, ListItemText } from '@mui/material';
import { HackerNewsItem } from '../api/hackernews.api';

type Props = {
  hits: HackerNewsItem[];
};

export default function Results({ hits }: Props) {
  return (
    <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
      <Divider />
      <List>
        {hits.map((item) => (
          <ListItem key={item.objectID} disablePadding>
            <ListItemButton component="a" href={item.url} target="_blank">
              <ListItemAvatar>
                <Avatar>
                  <OpenInNewIcon />
                </Avatar>
              </ListItemAvatar>
              <ListItemText primary={item.title} secondary={item.author} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
      <Divider />
    </Box>
  );
}
