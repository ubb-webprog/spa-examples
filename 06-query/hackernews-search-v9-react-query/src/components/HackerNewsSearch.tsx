import { Alert, CircularProgress, Divider } from '@mui/material';
import { useState } from 'react';
import { useHackerNewsHits } from '../query/hackernews.query';
import Results from './Results';
import SearchField from './SearchField';

export default function HackerNewsSearch() {
  const [query, setQuery] = useState('TypeScript');
  const { data, isLoading, isError, error } = useHackerNewsHits(query);

  return (
    <>
      <SearchField query={query} setQuery={setQuery} />
      <Divider />
      {isError && <Alert severity="error">Error: {error?.message}</Alert>}
      {isLoading ? <CircularProgress /> : <Results hits={data ?? []} />}
    </>
  );
}
