import axios from 'axios';

// axios példány közös beállításokkal
export const typicodeApi = axios.create({
  // közös kiinduló URL
  baseURL: 'https://jsonplaceholder.typicode.com',
  // közös headerek, pl. accept, token
  headers: {
    Accept: 'application/json',
  },
});

export type Todo = {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
};

export async function fetchAllTodos(): Promise<Todo[]> {
  const res = await typicodeApi.get<Todo[]>('/todos');
  return res.data.slice(0, 10);
}

export type CreateTodo = {
  title: string;
  completed: boolean;
};

export async function createTodo(data: CreateTodo): Promise<Todo> {
  const res = await typicodeApi.post<Todo>('/todos', data);
  return res.data;
}
