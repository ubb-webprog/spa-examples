import { useQuery } from '@tanstack/react-query';
import { fetchAllTodos } from '../api/todos.api';

export default function Todos() {
  const { data, isLoading, isError, error } = useQuery({
    queryKey: ['todos'],
    queryFn: fetchAllTodos,
  });

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (isError) {
    return <div className="error">{error?.message}</div>;
  }

  return (
    <>
      <h1>Todos</h1>
      <ul>
        {data?.map((todo) => (
          <li key={todo.id}>
            {todo.id}.&nbsp;
            {todo.title}
            {todo.completed && <i>&nbsp;(completed)</i>}
          </li>
        ))}
      </ul>
    </>
  );
}
