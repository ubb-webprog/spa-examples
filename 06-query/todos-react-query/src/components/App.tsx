import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import CreateTodoForm from './CreateTodoForm';
import Todos from './Todos';

import './style.css';

/**
 * Közös react-query kliens, ami együttműködik a context API-val
 * Beállítható kezdeti cache tartalom s közös kulcsok,
 * pl. mennyi időnként értékeljen újra, hányszor próbálkozzon újra hibák esetén, stb.
 */
export const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      // mennyi idő után minősüljön az adat elavultnak
      staleTime: 10000,
      // mennyi időnként értékeljen újra automatikusan
      refetchInterval: 30000,
    },
  },
});

export default function Root() {
  return (
    // react query inicializálás context API-n belül
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        {/* navbar */}
        <nav>
          <Link to="/todos">Todos</Link>
          <Link to="/create">Create</Link>
        </nav>
        <hr />
        <main>
          {/* URL szerint egy Route-ot rajzol ki a törzsbe */}
          <Routes>
            <Route path="/todos" element={<Todos />} />
            <Route path="/create" element={<CreateTodoForm />} />
            <Route path="*" element={<Todos />} />
          </Routes>
        </main>
      </BrowserRouter>
      {/* segédkomponens hogy monitorizáljuk a cache tartalmát */}
      <ReactQueryDevtools initialIsOpen={false} />
    </QueryClientProvider>
  );
}
