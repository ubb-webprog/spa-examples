import { useMutation, useQueryClient } from '@tanstack/react-query';
import { useState } from 'react';
import { createTodo } from '../api/todos.api';

export default function CreateTodoForm() {
  const [title, setTitle] = useState<string>('new todo');
  const [completed, setCompleted] = useState<boolean>(false);
  const queryClient = useQueryClient();

  const { mutate, data, isPending, isError, error } = useMutation({
    mutationFn: createTodo,
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['todos'] });
    },
  });

  return (
    <>
      <h1>Create todo</h1>
      <div>
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Title" />
      </div>
      <div>
        <input type="checkbox" checked={completed} onChange={() => setCompleted(!completed)} />
        &nbsp;Completed
      </div>
      <div>
        <input type="button" onClick={() => mutate({ title, completed })} value="Add new one" />
      </div>
      {isPending && (
        <div>
          <i>Adding...</i>
        </div>
      )}
      {isError && <div className="error">Error occurred: {error.message}</div>}
      {data && (
        <div>
          Created: <code>{JSON.stringify(data)}</code>
        </div>
      )}
    </>
  );
}
