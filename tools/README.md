# Webprog példaprogramokhoz segédkörnyezet

- szükséges Docker

## MySQL

- indítás: `docker compose -f mysql.docker-compose.yaml up --remove-orphans -d`
- leállítás: `docker compose  -f mysql.docker-compose.yaml down --remove-orphans`
- leállítás s adatok törlése: `docker compose  -f mysql.docker-compose.yaml down --remove-orphans --volumes`
- MySQL konzol indítás után: `docker exec -it webprog-mysql mysql -u root -pHnsAdrt0WtynpKJd -Dwebprog`

## MS SQL

- indítás: `docker compose -f mssql.docker-compose.yaml up --remove-orphans -d`
- leállítás: `docker compose  -f mssql.docker-compose.yaml down --remove-orphans`
- leállítás s adatok törlése: `docker compose  -f mssql.docker-compose.yaml down --remove-orphans --volumes`
- MS SQL konzol indítás után: `docker exec -it webprog-mssql-tools /opt/mssql-tools/bin/sqlcmd -S webprog-mssql -U sa -P HnsAdrt0WtynpKJd`

## MongoDB

- indítás: `docker compose -f mongo.docker-compose.yaml up --remove-orphans -d`
- leállítás: `docker compose  -f mongo.docker-compose.yaml down --remove-orphans`
- leállítás s adatok törlése: `docker compose  -f mongo.docker-compose.yaml down --remove-orphans --volumes`
- Online navigációs eszköz: `http://localhost:8081/`