/**
 * Az interface hasonló jelölő más OOP nyelvekhez
 */
interface Comparable {
  // ThisType: hivatkozás a típusra, amibe beágyazzuk a jelenlegit
  compareTo(other: ThisType<this>): number;
}

class NumberWrapper implements Comparable {
  val: number;

  constructor(val: number) {
    this.val = val;
  }

  // interfész metódus implementációja (ThisType helyett NumberWrapper szükséges)
  compareTo(other: NumberWrapper): number {
    return this.val - other.val;
  }
}

/**
 * Polimorfizmus
 * Interfész használata függvény-paraméterként
 */
function sortComparables(values: Array<Comparable>) {
  return values.sort((a, b) => a.compareTo(b));
}

console.log(sortComparables([new NumberWrapper(62), new NumberWrapper(19), new NumberWrapper(50)]));
