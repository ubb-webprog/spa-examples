/*
SAJÁT GENERIKUS TÍPUS
*/

// egy kételemű tömb, amelynek generikusan hivatkozunk a típusaira
// elegendő 1 generikus típust megadni, a második alapértelmezett értéke ugyanaz
type Pair<T1, T2 = T1> = [T1, T2];

// a két érték cseréje
function swap<T1, T2 = T1>(pair: Pair<T1, T2>): Pair<T2, T1> {
  const [v1, v2] = pair;
  return [v2, v1];
}

const p1: Pair<string> = ['hello', 'world'];
console.log(swap(p1)); // ['world', 'hello']

const p2: Pair<number, boolean> = [42, true];
console.log(swap(p2)); // [ true, 42 ]

/*
GENERIKUS TÖMB
*/

function log<T>(values: Array<T>) {
  for (let i = 0; i < values.length; i += 1) {
    console.log(`#${i}: ${values[i]}`);
  }
}

log([1, 2, 3]); // T = number
log(['hello', 'world']); // T = string
log([123, 'str']); // T = string | number
