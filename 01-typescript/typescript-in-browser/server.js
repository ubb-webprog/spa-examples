import express from 'express';

const app = express();

app.use(express.static('dist'));
app.use(express.static('public'));
app.use(express.static('src/public'));

app.listen(3000, () => console.log('Now serving: http://localhost:3000/ ...'));
