// számokat (s nem stringeket) garantáló sum
function sum(a: number, b: number): number {
  return a + b;
}

function getElementAsNumber(elementId: string): number {
  // A document elérhető kell legyen itt
  // A HTMLInputElement a type definition
  const element = document.getElementById(elementId) as HTMLInputElement;
  return Number(element.value);
}

function reevaluateSum() {
  console.log('reevaluating sums');
  const resultField = document.getElementById('result') as HTMLDivElement;

  const value1 = getElementAsNumber('number1');
  const value2 = getElementAsNumber('number2');

  resultField.innerText = sum(value1, value2).toString();
}

// DOM-elemre alkalmazott callback függvény típus
type CallbackFunction = (event: Event) => unknown;

// event típusok (amelyek megadhatóak az addEventListenernek)
type EventType = keyof HTMLElementEventMap;

function assignListener(elementId: string, eventType: EventType, callback: CallbackFunction) {
  const element = document.getElementById(elementId);
  element.addEventListener(eventType, callback);
}

window.onload = () => {
  assignListener('number1', 'change', reevaluateSum);
  assignListener('number2', 'change', reevaluateSum);
};
