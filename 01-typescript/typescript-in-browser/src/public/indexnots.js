// számokat (???) összeadó sum
function sum(a, b) {
  return a + b;
}

function getElement(elementId) {
  // A document elérhető kell legyen itt
  // A HTMLInputElement a type definition
  const element = document.getElementById(elementId);
  return element.value;
}

function reevaluateSum() {
  console.log('reevaluating sums');
  const resultField = document.getElementById('result');

  const value1 = getElement('number1');
  const value2 = getElement('number2');

  resultField.innerText = sum(value1, value2).toString();
}

function assignListener(elementId, eventType, callback) {
  const element = document.getElementById(elementId);
  element.addEventListener(eventType, callback);
}

window.onload = () => {
  assignListener('number1', 'change', reevaluateSum);
  assignListener('number2', 'change', reevaluateSum);
};
