import { getLines } from './lines';

console.log('Animals');

// top-level await
// feltétel: module és target beállítás a tsconfig.json-ban + "ts-node-esm" vagy "ts-node --esm" meghívás
(async () => {
  const lines = await getLines('animals.txt');
  lines.forEach((line) => console.log(line.toLowerCase()));
})();
