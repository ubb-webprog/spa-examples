// promise-osított fs műveletek
import fs from 'fs/promises';

/**
 * async függvény
 * mindig promise-t térít vissza
 * a generikus megmondja a feloldáskor visszatérített típust (lehet void is)
 */
export async function getLines(filename: string): Promise<string[]> {
  // az fs típusdefiniciói a @types/node függőségből érkeznek
  const buffer = await fs.readFile(filename);
  const lines = buffer.toString().split('\n');
  return lines;
}
