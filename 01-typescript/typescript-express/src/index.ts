import express, { Request, Response } from 'express';

const app = express();

type User = {
  id: number;
  name: string;
  age?: number;
};

//
// Response generikusok:
//   1. response body
//   2. locals
//

type UsersResponse = Response<User[]>;

app.get('/users', (req, res: UsersResponse) => {
  res.send([
    {
      id: 1,
      name: 'User 1',
      age: 42,
    },
    {
      id: 2,
      name: 'User 2',
    },
  ]);
});

//
// Request generikusok:
//   1. path params
//   2. response body
//   3. request body
//   4. query params
//   5. locals
//

type UserPathParams = { id: string };
type UserRequest = Request<UserPathParams, User>;
type UserResponse = Response<User>;

app.get('/users/:id', (req: UserRequest, res: UserResponse) => {
  const id = Number(req.params.id);

  if (id > 10) {
    res.sendStatus(404);
  }

  res.send({
    id,
    name: `User ${id}`,
  });
});

type UserCreation = Omit<User, 'id'>;
type CreateUserRequest = Request<never, never, UserCreation>;
type CreateUserResponse = Response<never>;

app.post('/users', express.json(), (req: CreateUserRequest, res: CreateUserResponse) => {
  console.log(`Creating user: ${req.body.name} (${req.body.age})`);
  res.send();
});

app.listen(5000, () => console.log('Listening on port 5000'));
