# Példaprogramok: Single Page és Progresszív webalkalmazások

- A tárgy laborfeladatainak sikeres megoldásához szükséges eszközök és módszertan megegyeznek a [Webprogramozás tárgyéival](https://gitlab.com/bbte-mmi/webprog/examples).
- A példaprogramok tematika szerint vannak csoportosítva. Minden tematikának egy külön könyvtár felel meg, ezen belül megtalálhatóak az aktuális példaprogramok.
