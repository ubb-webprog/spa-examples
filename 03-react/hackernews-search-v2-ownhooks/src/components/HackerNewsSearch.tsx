import { ChangeEvent, useCallback, useState } from 'react';
import useHackerNewsHits from '../hooks/useHackerNewsHits';

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function HackerNewsSearch() {
  // jelenlegi keresési fogalom, állapotban tartjuk
  const [query, setQuery] = useState('TypeScript');

  // saját hook használata
  const { hits, loading } = useHackerNewsHits(query);

  // cache-elt callback függvény (nincs belső függősége)
  const onQueryChange = useCallback((event: ChangeEvent<HTMLInputElement>) => setQuery(event.target.value), []);

  return (
    <>
      <h1>Hacker News Search V2</h1>
      <label htmlFor="searchHN">
        Search HackerNews articles:&nbsp;
        <input id="searchHN" type="text" value={query} onChange={onQueryChange} />
      </label>
      {loading && <i>Loading...</i>}
      <ul>
        {hits.map((item) => (
          <li key={item.objectID}>
            <a href={item.url}>{item.title}</a>
          </li>
        ))}
      </ul>
    </>
  );
}
