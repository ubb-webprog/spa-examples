import { useEffect, useState } from 'react';

// https://hn.algolia.com/api
export type HackerNewsItem = {
  objectID: string;
  title: string;
  url: string;
  author: string;
  points: number;
};

/**
 * Saját hook - meglévő hookok kombinációja
 */
export default function useHackerNewsHits(query: string) {
  // proxy másik hook felé
  const [hits, setHits] = useState<HackerNewsItem[]>([]);

  // komplexebb információ is beköthető, pl. hogy épp keresés zajlik-e
  const [loading, setLoading] = useState(true);

  // amikor megváltozik a keresési fogalom, újrafuttatjuk a keresést
  useEffect(() => {
    setLoading(true);
    fetch(`https://hn.algolia.com/api/v1/search?query=${query}`)
      .then((response) => response.json())
      .then((result) => {
        setHits(result.hits);
        setLoading(false);
      });
  }, [query]);

  return { hits, loading };
}
