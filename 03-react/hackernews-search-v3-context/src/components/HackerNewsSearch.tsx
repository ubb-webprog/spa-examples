import { useMemo, useState } from 'react';
import SearchField from './SearchField';
import { SearchTermContext } from '../context/searchterm.context';
import Results from './Results';

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function HackerNewsSearch() {
  // jelenlegi keresési fogalom, állapotban tartjuk
  const [query, setQuery] = useState('TypeScript');

  // a context jelenlegi értékét memoizáljuk
  const context = useMemo(() => ({ query }), [query]);

  return (
    <>
      <h1>Hacker News Search V3</h1>
      <SearchField query={query} setQuery={setQuery} />

      <SearchTermContext.Provider value={context}>
        <Results />
      </SearchTermContext.Provider>
    </>
  );
}
