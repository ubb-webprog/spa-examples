import { ChangeEvent, useCallback, useEffect, useState } from 'react';

// https://hn.algolia.com/api
type HackerNewsItem = {
  objectID: string;
  title: string;
  url: string;
  author: string;
  points: number;
};

// Komponens amely keresést végez a HackerNews Algolia API-n
export default function HackerNewsSearch() {
  // jelenlegi keresési fogalom, állapotban tartjuk
  const [query, setQuery] = useState('TypeScript');

  // találatok
  const [hits, setHits] = useState<HackerNewsItem[]>([]);

  // amikor megváltozik a keresési fogalom, újrafuttatjuk a keresést
  useEffect(() => {
    fetch(`https://hn.algolia.com/api/v1/search?query=${query}`)
      .then((response) => response.json())
      .then((result) => setHits(result.hits));
  }, [query]);

  // cache-elt callback függvény (nincs belső függősége)
  const onQueryChange = useCallback((event: ChangeEvent<HTMLInputElement>) => setQuery(event.target.value), []);

  return (
    <>
      <h1>Hacker News Search V1</h1>
      <label htmlFor="searchHN">
        Search HackerNews articles:&nbsp;
        <input id="searchHN" type="text" value={query} onChange={onQueryChange} />
      </label>
      <ul>
        {hits.map((item) => (
          <li key={item.objectID}>
            <a href={item.url}>{item.title}</a>
          </li>
        ))}
      </ul>
    </>
  );
}
